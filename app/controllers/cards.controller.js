'use strict';

/**
 * Module dependencies.
 */
const knex = require('../../config/lib/knex').db;
const config = require('../../config/config');

import Response from '../../config/lib/response';


class CardsController extends Response {
  async listDesks({user}) {
    return await knex
      .select(knex.raw('COUNT(desk) as desk_count'), 'desk', knex.raw('MIN(created_at) as created_at_min'))
      .where({
        user_id: user.id
      })
      .from('cards')
      .groupBy('desk')
      .orderBy('desk_count', 'ASC')
  }

  async listCardsForDesk({user, params}) {
    return await knex
      .select('cards.original as original_word_id', 'translation as translation_word_id', 'id',
        knex.raw(`coalesce((select COUNT(repeats.id) as translation_success
          from repeats where repeats.repeat_status_id = 1 and cards.id = repeats.card_id and cards.translation = repeats.word_id), 0) as translation_success`),
        knex.raw(`coalesce((select COUNT(repeats.id) as original_success
          from repeats where repeats.repeat_status_id = 1 and cards.id = repeats.card_id and cards.original = repeats.word_id), 0) as original_success`),
        knex.raw(`coalesce((select COUNT(repeats.id) as translation_unbeaten
          from repeats where repeats.repeat_status_id = 1 and  cards.id = repeats.card_id and cards.translation = repeats.word_id
          and ((select MAX(repeats.created_at)
          from repeats where repeats.repeat_status_id = 2 and
          cards.id = repeats.card_id and cards.translation = repeats.word_id) is null or (select MAX(repeats.created_at)
          from repeats where repeats.repeat_status_id = 2 and
          cards.id = repeats.card_id and cards.translation = repeats.word_id) < repeats.created_at)), 0) as translation_unbeaten`),
        knex.raw(`coalesce((select COUNT(repeats.id) as original_unbeaten
          from repeats where repeats.repeat_status_id = 1 and  cards.id = repeats.card_id and cards.original = repeats.word_id
          and ((select MAX(repeats.created_at)
          from repeats where repeats.repeat_status_id = 2 and
          cards.id = repeats.card_id and cards.original = repeats.word_id) is null or (select MAX(repeats.created_at)
          from repeats where repeats.repeat_status_id = 2 and
          cards.id = repeats.card_id and cards.original = repeats.word_id) < repeats.created_at)), 0) as original_unbeaten`),
        knex.raw(`(select word from words where cards.original = words.id) as original`),
        knex.raw(`(select word from words where cards.translation = words.id) as translation`)
      )
      .where({
        user_id: user.id,
        desk: params.desk
      })
      .groupBy('cards.id')
      .from('cards');
  }

  /**
   * Crete repeat record
   */
  async createRepeat({params, user}) {
    let repeatStatusId = 1;
    let desk = params.desk;

    if (params.repeatStatus === 'fail') {
      repeatStatusId = 2;
    }

    let repeats = await knex('repeats')
      .returning(['card_id', 'word_id'])
      .insert({
        card_id: params.cardId,
        word_id: params.wordId,
        repeat_status_id: repeatStatusId
      });

    if (repeatStatusId === 1) {
      desk = await this.combineRepeats(user, {desk: parseInt(params.desk, 10)});
    }


    return {
      card_id: repeats[0].card_id,
      word_id: repeats[0].word_id,
      desk: desk
    };
  }

  /**
   * Combine 3 desk in one big desk
   */
  async combineRepeats({id}, {desk}) {
    let currentLevelDesk;

    let desks = await knex
      .select('desk',
        knex.raw(`SUM(coalesce((select COUNT(repeats.id)
          from repeats where repeats.repeat_status_id = 1 and cards.id = repeats.card_id and cards.translation = repeats.word_id), 0)) as translation_success`),
        knex.raw(`SUM(coalesce((select COUNT(repeats.id)
          from repeats where repeats.repeat_status_id = 1 and cards.id = repeats.card_id and cards.original = repeats.word_id), 0)) as original_success`),
        knex.raw('COUNT(cards.id) as count_cards'),
        knex.raw('(select MAX(desk) from cards) as desk_max')
      )
      .where({
        user_id: id
      })
      .groupBy('cards.desk')
      .from('cards');

    let levels = {};

    if (!desks.length) return desk;

    desks.forEach((sqlDesk) => {
      let repeatOneTranslationCard = Math.floor(parseInt(sqlDesk.translation_success, 10) / parseInt(sqlDesk.count_cards, 10));
      let repeatOneOriginalCard = Math.floor(parseInt(sqlDesk.original_success, 10) / parseInt(sqlDesk.count_cards, 10));

      if (parseInt(sqlDesk.count_cards, 10) === 10) {
        if (repeatOneTranslationCard >= 3 && repeatOneOriginalCard >= 3) {
          if (!levels[0]) levels[0] = [];

          levels[0].push(sqlDesk.desk);

          if (sqlDesk.desk === desk) currentLevelDesk = 0;
        }
      } else if (((parseInt(sqlDesk.count_cards, 10) % 3) === 0) && ((parseInt(sqlDesk.count_cards, 10) % 10) === 0)) {
        let currentLevel = parseInt(sqlDesk.count_cards, 10) / 3 / 10;

        if (repeatOneTranslationCard  >= (currentLevel + 3)
          && (repeatOneOriginalCard >= (currentLevel + 3))) {
          if (!levels[currentLevel]) levels[currentLevel] = [];

          levels[currentLevel].push(sqlDesk.desk);

          if (sqlDesk.desk === desk) currentLevelDesk = currentLevel;
        }
      }
    });

    if ((typeof currentLevelDesk === 'number') && levels[currentLevelDesk].length >= 3) {
      levels[currentLevelDesk].length = 3;
      let newDesk = parseInt(desks[0].desk_max, 10) + 1;

      await knex('cards')
        .whereIn('desk', levels[currentLevelDesk])
        .update({
          desk: newDesk
        });

      return newDesk;
    }

    return desk;
  }

  async create({body, user}) {
    let originalId;
    let translationId;
    let deskNumberCards = 1;
    let maxCardsInDesk = 10;
    let newDesk = 1;

    let [card] = await knex.transaction(trx => {
      return trx
        .select('*')
        .from('words')
        .where({
          user_id: user.id,
          word: body.original
        })
        .limit(1)
        .then(words => {
          if (words.length) {
            return words;
          }

          return trx
            .returning(['id'])
            .insert({
              word: body.original,
              user_id: user.id
            })
            .into('words');
        }).then(words => {
          originalId = words[0].id;

          return trx
            .select('*')
            .from('words')
            .where({
              user_id: user.id,
              word: body.translation
            })
            .limit(1);
        }).then(words => {
          if (words.length) {
            return words;
          }

          return trx
            .returning(['id'])
            .insert({
              word: body.translation,
              user_id: user.id
            })
            .into('words');
        }).then(words => {
          translationId = words[0].id;

          return trx
            .select(knex.raw('COUNT(desk) as desk_number_cards'), 'desk', knex.raw('(select MAX(desk) from cards) as desk_max'))
            .where({
              user_id: user.id
            })
            .from('cards')
            .groupBy('desk')
            .orderBy('desk_number_cards', 'ASC')
            .limit(1);
        }).then((desks) => {
          let desk = desks[0];

          if (desk) {
            if (parseInt(desk.desk_number_cards, 10) < maxCardsInDesk) {
              newDesk = desk.desk;
              deskNumberCards = parseInt(desk.desk_number_cards, 10) + 1;
            } else {
              newDesk = desk.desk_max + 1;
            }
          }

          return trx
            .returning(['id', 'original', 'translation', 'desk'])
            .insert({
              original: originalId,
              translation: translationId,
              user_id: user.id,
              desk: newDesk
            })
            .into('cards');
        });
    });

    card.desk_number_cards = deskNumberCards;

    return card;
  }

}

export default new CardsController();
