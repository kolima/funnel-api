'use strict';

/**
 * Methods of the core
 */
class CoreController {
  getToken(authorization = '') {
    let parts = authorization.split(' ');

    if (parts.length !== 2) {
      throw Error('Format isn\'t Authorization: Bearer [token]');
    }

    let scheme = parts[0];
    let credentials = parts[1];

    if (/^Bearer$/i.test(scheme)) {
      return credentials;
    } else {
      throw Error('Format isn\'t Authorization: Bearer [token]');
    }
  }
}

export default new CoreController();
