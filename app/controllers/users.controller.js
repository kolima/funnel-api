'use strict';

/**
 * Module dependencies.
 */
const knex = require('../../config/lib/knex').db;
const config = require('../../config/config');
const usersEmail = require('../emails/users.email');
const jwt = require('jsonwebtoken');

import Response from '../../config/lib/response';


class UsersController extends Response {
  /**
   * Signin user
   */
  async signin({body}) {
    const result = await knex.select('id', 'email', 'name').from(function () {
      this.select('users.id as id', 'email', 'name',
        knex.raw(`(hash_password = crypt('${body.password}', hash_password)) as is_same_passwords`))
        .from('users')
        .where({
          email: body.email
        })
        .as('ignored_alias')
        .limit(1);
    }).where({
      is_same_passwords: true
    });

    if (!result[0]) {
      return {
        data: {message: 'Invalid email or password'},
        code: 400
      };
    }

    return {
      token: jwt.sign(result[0], config.jwt.secret, {expiresIn: config.jwt.ttl}),
      ttl: config.jwt.ttl
    };
  }

  /**
   * Create user
   */
  async createByToken({body}) {
    let userId;

    let result = await knex.transaction(async (trx) => {
      const ids = await trx
        .insert({
          email: body.email,
          name: body.name
        }, 'id')
        .into('users');

      userId = ids[0];

      return await trx
        .returning('token')
        .insert({
          user_id: userId,
          token: knex.raw('gen_random_uuid()')
        })
        .into('emails_tokens');
    });

    usersEmail.inviteNewAdmin({
      name: body.name,
      email: body.email,
      link: `${config.clientUrl}/password/create?token=${result[0]}`
    });

    return {
      id: userId,
      email: body.email,
      name: body.name
    }
  }

  async handlePassword(body, emailTokenTypeId) {
    let emailTokens = await knex('emails_tokens')
      .select('token', 'user_id')
      .where({
        token: body.token,
        email_token_type_id: emailTokenTypeId
      })
      .innerJoin('users', 'users.id', 'emails_tokens.user_id');

    if (!emailTokens.length) {
      return {
        data: {message: 'Authorization error'},
        code: 401
      };
    }


    let users = await knex('users')
      .returning(['id', 'email', 'name'])
      .where({
        id: emailTokens[0].user_id
      })
      .update('hash_password', knex.raw(`crypt('${body.password}', gen_salt(\'md5\'))`));

    await knex('emails_tokens')
      .where({
        user_id: users[0].id,
        email_token_type_id: emailTokenTypeId
      })
      .del();

    return {
      token: jwt.sign(users[0], config.jwt.secret, {expiresIn: config.jwt.ttl}),
      ttl: config.jwt.ttl
    };
  }

  /**
   * Create token for set up password
   */
  async createPassword({body}) {
    return await this.handlePassword(body, 1);
  }

  async updatePassword({body}) {
    return await this.handlePassword(body, 2);
  }

  async getMe({user}) {
    const users = await knex('users')
      .select('id', 'name', 'email')
      .where({
        id: user.id
      })
      .limit(1);

    return users[0];
  }

  async updateMe({user, body}) {
    let users;
    let returning = ['id', 'email', 'name'];

    if (!body.password) {
      users = await knex('users')
        .returning(returning)
        .where({
          id: user.id
        })
        .update({
          name: body.name
        }).limit(1);


    } else {
      users = await knex('users')
        .returning(returning)
        .where({
          id: user.id
        })
        .update({
          hash_password: knex.raw(`crypt('${body.password}', gen_salt(\'md5\'))`),
          name: body.name
        }).limit(1);
    }


    return {
      token: jwt.sign(users[0], config.jwt.secret, {expiresIn: config.jwt.ttl}),
      ttl: config.jwt.ttl
    };
  }


  async forgotPassword({body}) {
    const users = await knex('users')
      .select('id', 'name')
      .where({
        email: body.email
      })
      .limit(1);

    if (!users.length) {
      return {
        data: {message: 'Not found email'},
        code: 400
      };
    }

    const tokens = await knex
      .returning('token')
      .insert({
        user_id: users[0].id,
        token: knex.raw('gen_random_uuid()'),
        email_token_type_id: 2
      })
      .into('emails_tokens');

    usersEmail.recoveryPassword({
      name: users[0].name,
      email: body.email,
      link: `${config.clientUrl}/password/edit?token=${tokens[0]}`
    });

    return {
      email: body.email
    };
  }
}

export default new UsersController();
