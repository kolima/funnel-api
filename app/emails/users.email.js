'use strict';

const mandrill = require('mandrill-api/mandrill');
const config = require('../../config/config');
const client = new mandrill.Mandrill(config.mandrillAPIKey);

class UsersEmailTemplates {
  inviteNewUser({name, email, link}) {
    const content = `<p>Dear ${name},</p><br> <p>You have been added as an user to Funnel system.</p>` +
      `<p>Click on the following link to set a password and get access your account:</p> <p>${link}</p>`;

    return this.send(content, 'Funnel Invitation ✔', email);
  }

  recoveryPassword({name, email, link}) {
    const content = `<p>Dear ${name},</p><br> <p>You have requested the recovery password in Funnel system.</p>` +
      `<p>Click on the following link to reset your password:</p> <p>${link}</p>`;

    return this.send(content, 'Funnel Recovery ✔', email);
  }

  send(content, subject, recipient) {
    const message = {
      html: content,
      subject: subject,
      from_email: 'no-reply@funnel.com',
      from_name: 'Funnel',
      to: [{
        email: recipient,
        type: 'to'
      }]
    };

    return new Promise((resolve, reject) => {
      return client.messages.send({message: message, async: true}, (result) => {
        return reject(result);
      }, (err) => {
        return reject(err);
      });
    });


  }
}

export default new UsersEmailTemplates();
