'use strict';

module.exports = (table, knex) => {
  table.increments('id').notNullable().primary();
  table.integer('original').unsigned().notNullable().references('words.id').onDelete('cascade');
  table.integer('translation').unsigned().notNullable().references('words.id').onDelete('cascade');
  table.integer('desk').unsigned().notNullable();
  table.uuid('user_id').notNullable().references('users.id').onDelete('cascade');
  table.timestamp('created_at', true).defaultTo(knex.raw('now()')).notNullable();
  table.timestamp('updated_at', true).defaultTo(knex.raw('now()')).notNullable();
  table.unique(['original', 'translation', 'user_id']);
};
