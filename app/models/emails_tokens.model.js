'use strict';

module.exports = (table, knex) => {
  table.increments('id').notNullable().primary();
  table.uuid('token').notNullable();
  table.uuid('user_id').notNullable().references('users.id').onDelete('cascade');
  table.integer('email_token_type_id').unsigned().notNullable().references('emails_tokens_types.id').onDelete('cascade');
  table.timestamp('created_at', true).defaultTo(knex.raw('now()')).notNullable();
};
