'use strict';

module.exports = (table) => {
  table.increments('id').notNullable().primary();
  table.string('title').notNullable().unique();
};
