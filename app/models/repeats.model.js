module.exports = (table, knex) => {
  table.increments('id').notNullable().primary();
  table.integer('card_id').unsigned().notNullable().references('cards.id').onDelete('cascade');
  table.integer('word_id').unsigned().notNullable().references('words.id').onDelete('cascade');
  table.integer('repeat_status_id').unsigned().notNullable().references('repeats_statuses.id').onDelete('cascade');
  table.timestamp('created_at', true).defaultTo(knex.raw('now()')).notNullable();
};
