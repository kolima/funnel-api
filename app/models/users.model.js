'use strict';

module.exports = (table, knex) => {
  table.uuid('id').defaultTo(knex.raw('gen_random_uuid()')).notNullable().primary();
  table.string('name').notNullable();
  table.string('email').notNullable().unique();
  table.string('hash_password');
  table.timestamp('created_at', true).defaultTo(knex.raw('now()')).notNullable();
  table.timestamp('updated_at', true).defaultTo(knex.raw('now()')).notNullable();
};


