'use strict';

module.exports = (table, knex) => {
  table.increments('id').notNullable().primary();
  table.string('word').notNullable();
  table.uuid('user_id').notNullable().references('users.id').onDelete('cascade');
  table.timestamp('created_at', true).defaultTo(knex.raw('now()')).notNullable();
  table.timestamp('updated_at', true).defaultTo(knex.raw('now()')).notNullable();
  table.unique(['word', 'user_id']);
};
