'use strict';

const controller = require('../controllers/cards.controller.js');
const validation = require('../validations/cards.validation.js');

module.exports = (app) => {

  app.route('/cards')
    .post(controller.method('create'));

  app.route('/desks')
    .get(controller.method('listDesks'));

  app.route('/desks/:desk/cards')
    .get(controller.method('listCardsForDesk'));

  app.route('/desks/:desk/cards/:cardId/words/:wordId/repeats/:repeatStatus')
    .post(controller.method('createRepeat'))
};
