'use strict';

const controller = require('../controllers/users.controller.js');
const validation = require('../validations/users.validation.js');

module.exports = (app) => {
  // Signin
  app.route('/auth/signin')
    .post(validation.method('signin'), controller.method('signin'));

  app.route('/auth/password')
    .post(validation.method('handlePassword'), controller.method('createPassword'))
    .put(validation.method('handlePassword'), controller.method('updatePassword'));

  app.route('/auth/password/forgot')
    .post(validation.method('forgotPassword'), controller.method('forgotPassword'));

  app.route('/users/me')
    .put(validation.method('updateMe'), controller.method('updateMe'))
    .get(controller.method('getMe'));

  app.param('userId', validation.method('userId'));
};
