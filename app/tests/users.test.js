'use strict';

var should = require('should'),
  request = require('supertest'),
  path = require('path'),
  config = require(path.resolve('config/config')),
  knex = require(path.resolve('config/lib/knex')).db,
  express = require(path.resolve('config/lib/express')),
  jwt = require('jsonwebtoken');

/**
 * Globals
 */
var agent, app, token;

/**
 * Companies routes tests
 */
describe('Users tests', function () {
  before(function (done) {
    // Get application
    app = express.init();
    agent = request.agent(app);
    done();
  });

  it('success email, password in signin', function (done) {
    agent.post('/auth/signin')
      .send({
        email: 'nbezruk@qualium-systems.com.ua',
        password: 'qwerty'
      })
      .expect(200)
      .end(function (err, result) {
        if (err) {
          return done(err);
        }

        const data = JSON.parse(result.text);

        token = data.token;

        should(data).have.keys('token', 'ttl');

        return done();
      });
  });

  it('wrong password in signin', function (done) {
    agent.post('/auth/signin')
      .send({
        email: 'nbezruk@qualium-systems.com.ua',
        password: 'qwert1'
      })
      .expect(400)
      .end(function (err, result) {
        if (err) {
          return done(err);
        }


        should(result.body).have.keys('message');

        return done();
      });
  });

  it('wrong email in signin', function (done) {
    agent.post('/auth/signin')
      .send({
        email: 'nbezruk@qualium-systems.com.u',
        password: 'qwerty'
      })
      .expect(400)
      .end(function (err, result) {
        if (err) {
          return done(err);
        }

        should(result.body).have.keys('message', 'errors');

        return done();
      });
  });

  it('get me profile', function (done) {
    agent.get('/users/me')
      .set('Authorization', `Bearer ${token}`)
      .expect(200)
      .end(function (err, result) {
        if (err) {
          return done(err);
        }

        should(result.body).have.keys('id', 'email' , 'name');

        return done();
      });
  });
});
