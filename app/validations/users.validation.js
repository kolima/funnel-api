'use strict';

import Response from '../../config/lib/response';


class UsersValidation extends Response {
  async signin({checkBody, validationErrors}) {
    checkBody({
      email: {
        notEmpty: true,
        isEmail: {
          errorMessage: 'Invalid email'
        },
        isLength: {
          options: [0, 255],
          errorMessage: 'Must be less than 255 chars long'
        }
      },
      password: {
        notEmpty: true,
        isLength: {
          options: [2, 30],
          errorMessage: 'Must be between 2 and 30 chars long'
        },
        errorMessage: 'Invalid password'
      }
    });

    return await this.getValidationError(validationErrors());
  }

  async handlePassword({checkBody, validationErrors}) {
    checkBody({
      token: {
        notEmpty: true,
        isUUID: {
          errorMessage: 'Invalid token'
        }
      },
      password: {
        notEmpty: true,
        isLength: {
          options: [2, 30],
          errorMessage: 'Must be between 2 and 30 chars long'
        },
        errorMessage: 'Invalid password'
      }
    });

    return await this.getValidationError(validationErrors());
  }

  async forgotPassword({checkBody, validationErrors}) {
    checkBody({
      email: {
        notEmpty: true,
        isEmail: {
          errorMessage: 'Invalid email'
        },
        isLength: {
          options: [0, 255],
          errorMessage: 'Must be less than 255 chars long'
        }
      }
    });

    return await this.getValidationError(validationErrors());
  }

  async updateMe({checkBody, validationErrors}) {
    checkBody({
      name: {
        notEmpty: true,
        isLength: {
          options: [0, 255],
          errorMessage: 'Must be less than 255 chars long'
        }
      },
      password: {
        optional: true,
        isLength: {
          options: [2, 30],
          errorMessage: 'Must be between 2 and 30 chars long'
        },
        errorMessage: 'Invalid password'
      }
    });

    return await this.getValidationError(validationErrors());
  }

  async userId({checkParams, validationErrors}) {
    checkParams({
      adminId: {
        notEmpty: true,
        isUUID: {
          errorMessage: 'Invalid userId'
        }
      }
    });

    return await this.getValidationError(validationErrors());
  }
}

export default new UsersValidation();
