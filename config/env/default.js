'use strict';

module.exports = {
  app: {
    title: 'Funnel API'
  },

  port: process.env.PORT || 3010,

  db: {
    migration: {
      table: 'migrations',
      folder: 'migrations'
    }
  },
  clientUrl: 'https://kolima-funnel-client.herokuapp.com',
  mandrillAPIKey: '',
  jwt: {
    secret: 'funnel-api',
    ttl: 180 * 24 * 60 * 60
  },
  files: {
    server: {
      models: 'app/models/**/*.js',
      policies: 'app/policies/**/*.js',
      routes: 'app/routes/**/*.js',
      controllers: 'app/controllers/**/*.js'
    },
    config: 'config/**/*.js'
  }
};
