'use strict';

module.exports = {
  db: {
    uri: process.env.FUNNEL_POSTGRES_URI,
    options: {
      dialect: 'pg',
      debug: false,
      ssl: true
    }
  },
  log: {
    // Can specify one of 'combined', 'common', 'dev', 'short', 'tiny'
    format: 'dev',
    // Stream defaults to process.stdout
    // Uncomment to enable logging to a log on the file system
    options: {
      //stream: 'access.log'
    }
  }
};
