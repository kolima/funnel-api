'use strict';

module.exports = {
  db: {
    uri: process.env.FUNNEL_POSTGRES_URI,
    options: {
      dialect: 'pg',
      ssl: true
    }
  },
  log: {
    // Can specify one of 'combined', 'common', 'dev', 'short', 'tiny'
    format: 'dev',
    options: {
      //stream: 'access.log'
    }
  }
};
