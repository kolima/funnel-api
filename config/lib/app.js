'use strict';

/**
 * Module dependencies.
 */
var config = require('../config'),
  knex = require('./knex'),
  express = require('./express'),
  chalk = require('chalk');


// Initialize Database
export function init(callback) {

  knex.loadModels()
    .then((db) => {
      // Initialize express
      let app = express.init(db);

      if (callback) callback(app, db, config);
    })
    .catch((err) => {
      console.error(chalk.red(err.message));
      process.exit(-1);
    });
}


// Start app
export function start() {

  this.init(function (app, db, config) {

    // Start the app by listening on <port>
    app.listen(config.port, () => {

      // Logging initialization
      console.log('--');
      console.log(chalk.green(config.app.title));
      console.log(chalk.green('Environment:\t\t\t' + process.env.NODE_ENV));
      console.log(chalk.green('Port:\t\t\t\t' + config.port));
      console.log(chalk.green('Database:\t\t\t' + config.db.uri));

      if (config.secure && config.secure.ssl === 'secure') {
        console.log(chalk.green('HTTPs:\t\t\t\ton'));
      }

      console.log('--');
    });
  });

}

