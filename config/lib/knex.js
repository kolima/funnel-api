'use strict';

/**
 * Module dependencies.
 */
var config = require('../config'),
  chalk = require('chalk'),
  path = require('path'),
  types = require('pg').types,
  moment = require('moment'),
  url = require('url'),
  knex = require('knex');

// Parse url server
const pgUrl = url.parse(config.db.uri);

const TIMESTAMPTZ_OID = 1184;
const TIMESTAMP_OID = 1114;

function parseFn(val) {
  return val === null ? null : moment.utc(val).toDate();
}
types.setTypeParser(TIMESTAMPTZ_OID, parseFn);
types.setTypeParser(TIMESTAMP_OID, parseFn);

// Initialize Knex
const db = knex({
  client: config.db.options.dialect,
  debug: config.db.options.debug,
  connection: {
    host: pgUrl.hostname,
    port: pgUrl.port,
    user: pgUrl.auth.split(':')[0],
    password: pgUrl.auth.split(':')[1],
    database: pgUrl.path.substring(1),
    ssl: config.db.options.ssl
  },
  migrations: {
    directory: path.resolve() + '/' + config.db.migration.folder,
    tableName: config.db.migration.table
  }
});

// Get pattern for references in model
function getRefPattern(name) {
  if (name) {
    return `references\\(\\'${name}`;
  }
  return `references\\(\\'`;
}

// Connection
module.exports.db = db;

// Load the knex models
module.exports.loadModels = () => {

  let models = config.files.server.models.map((modelPath) => {
    return {
      path: modelPath,
      name: path.basename(modelPath, '.model.js'),
      model: require(path.resolve(modelPath)).toString()
    };
  });


  let pattern = new RegExp(getRefPattern(), 'g');

  models.sort((a, b) => {
    if (pattern.test(a.model) && pattern.test(b.model)) {
      if (new RegExp(getRefPattern(b.name), 'g').test(a.model)) {
        return 1;
      } else if (new RegExp(getRefPattern(a.name), 'g').test(b.model)) {
        return -1;
      } else {
        return 0;
      }
    } else if (pattern.test(a.model) && !pattern.test(b.model)) {
      return 1;
    } else if (!pattern.test(b.model) && pattern.test(b.model)) {
      return -1;
    } else {
      return 0;
    }
  });

  return Promise.all(models.map(function (model) {
    return db.schema.hasTable(model.name).then((exists) => {
      if (exists) return null;

      return db.schema.createTable(model.name, (table) => {
        require(path.resolve(model.path))(table, knex);
      });
    });
  }));
};


