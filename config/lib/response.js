'use strict';

const chalk = require('chalk');

/**
 * Response methods
 */
export default class Response {
  getValidationError(errors) {
    if (!errors.length) {
      return 'next';
    }

    return {
      code: 400,
      data: {
        message: 'Validation Failed',
        errors: errors
      }
    };
  }


  /**
   * Method return result for request
   * todo: add new.target.name for chalk
   */
  method(name) {

    if (!this[name]) {
      console.error(chalk.red(`+ Error: Method ${name} is missing`));
    }

    return async (req, res, next) => {
      try {
        const result = await this[name]({...req});

        if (result === 'next') {
          return next();
        }

        res.status(result.code || 200).json(result.data || result);
      } catch (err) {

        let message, errors;

        switch (parseInt(err.code, 10)) {
          case 23505:
            if (/competitions/.test(req.originalUrl)) {
              message = 'Validation failed';

              errors = [{
                param: 'own_url',
                msg: 'Url already exists'
              }];
            } else {
              message = 'Email already exists';
            }
            break;
          default:
            message = process.env.NODE_ENV === 'production' ? 'Bad request' : err.message;
            break;
        }

        next({message, errors});
      }
    };
  }
}
