
exports.up = function(knex, Promise) {
  return knex('users').insert({
    name: 'Nikolai',
    email: 'nbezruk@qualium-systems.com.ua',
    hash_password: knex.raw('crypt(\'qwerty\', gen_salt(\'md5\'))')
  });
};

exports.down = function(knex, Promise) {

};
